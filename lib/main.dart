import 'package:flutter/material.dart';
import 'package:to_my_health/Fitness_Page.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: "To My Health",
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var fitness = ['-', '-'];

  @override
  void initState() {
    super.initState();
    _loadFitness();
  }

  Future<void> _loadFitness() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      fitness = prefs.getStringList('fitness_value') ?? ['-', '-'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'To My Health',
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
        backgroundColor: Colors.deepPurple.shade200,
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: [
          Card(
            child: Column(
              children: [
                ListTile(
                  leading: Icon(
                    Icons.account_balance_wallet_outlined,
                    color: Colors.purple,
                  ),
                  title: Text('น้ำหนัก'),
                  subtitle: Text(
                    '0 kg',
                    style: TextStyle(fontSize: 20, color: Colors.teal),
                  ),
                ),
              ],
            ),
          ),
          Card(
            child: Column(
              children: [
                ListTile(
                  leading: Icon(
                    Icons.food_bank_outlined,
                    color: Colors.purple,
                  ),
                  title: Text('อาหาร'),
                  subtitle: Text(
                    '0 แคลอรี่่',
                    style: TextStyle(fontSize: 20, color: Colors.teal),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.all(15),
                        child: Text(
                          'บริมาณแคลอรี่ที่ต้องการต่อวัน 1403 แคลอรี่',
                          style: TextStyle(fontSize: 15, color: Colors.brown),
                        )),
                  ],
                )
              ],
            ),
          ),
          Card(
            child: Column(
              children: [
                ListTile(
                  leading: Icon(
                    Icons.accessibility_new_outlined,
                    color: Colors.purple,
                  ),
                  title: Text('การออกกำลังกาย'),
                  subtitle: Text(
                    '0 นาที',
                    style: TextStyle(fontSize: 20, color: Colors.teal),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.all(15),
                        child: Text(
                          'ประเภทการออกลังกาย ${fitness.toList()}',
                          style: TextStyle(fontSize: 15, color: Colors.brown),
                        )),
                    Padding(
                        padding: const EdgeInsets.all(15),
                        child: Text(
                          'จำนวนพลังงานที่เผาผลาญ',
                          style: TextStyle(fontSize: 15, color: Colors.brown),
                        )),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
      drawer: Drawer(
          child: Container(
        color: Colors.deepPurple.shade50,
        child: ListView(
          children: [
            DrawerHeader(
              child: Text(
                'เมนู',
                style: TextStyle(fontSize: 30, color: Colors.black),
              ),
              decoration: BoxDecoration(color: Colors.deepPurple.shade100),
            ),
            ListTile(
              title: Text('น้ำหนัก'),
              subtitle: Text('บันทึกน้ำหนัก'),
              onTap: () {},
            ),
            ListTile(
              title: Text('อาหาร'),
              subtitle: Text('บันทึกมื้ออาหาร'),
              onTap: () {},
            ),
            ListTile(
              title: Text('ประเภทการออกกำลังกาย'),
              subtitle: Text('เลือกประเภทการออกกำลังกาย'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Fitness()));
              },
            ),
            ListTile(
              title: Text('เวลา'),
              subtitle: Text('บันทึกเวลาที่ใช้ในการออกกำลังกาย'),
              onTap: () {},
            ),
          ],
        ),
      )),
      backgroundColor: Colors.deepPurple.shade50,
    );
  }
}
