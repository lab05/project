import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Fitness extends StatefulWidget {
  Fitness({Key? key}) : super(key: key);

  @override
  _FitnessState createState() => _FitnessState();
}

class _FitnessState extends State<Fitness> {
  var fitness = [
    'กระโดดตบ',
    'กระโดดเชือก',
    'ปั่นจักรยาน',
    'ซิทอัพ',
    'พิลาทิส',
    'วิดพื้น',
    'สควอท',
    'แพลงก์',
    'แอโรบิค',
    'วิ่ง',
    'ฮูล่าฮูป',
    'ว่ายน้ำ'
  ];

  var fitnessValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'ประเภทการออกกำลังกาย',
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
        backgroundColor: Colors.deepPurple.shade200,
      ),
      body: Container(
        child: ListView(
          children: [
            CheckboxListTile(
                title: Text(fitness[0]),
                value: fitnessValue[0],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[0] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[1]),
                value: fitnessValue[1],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[1] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[2]),
                value: fitnessValue[2],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[2] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[3]),
                value: fitnessValue[3],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[3] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[4]),
                value: fitnessValue[4],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[4] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[5]),
                value: fitnessValue[5],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[5] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[6]),
                value: fitnessValue[6],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[6] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[7]),
                value: fitnessValue[7],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[7] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[8]),
                value: fitnessValue[8],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[8] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[9]),
                value: fitnessValue[9],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[9] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[10]),
                value: fitnessValue[10],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[10] = newValue!;
                  });
                }),
            CheckboxListTile(
                title: Text(fitness[11]),
                value: fitnessValue[11],
                onChanged: (newValue) {
                  setState(() {
                    fitnessValue[11] = newValue!;
                  });
                }),
          ],
        ),
      ),
      backgroundColor: Colors.deepPurple.shade50,
    );
  }

  @override
  void initState() {
    super.initState();
    _loadFitness();
  }

  Future<void> _loadFitness() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('fitness_value') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        fitnessValue[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });

    Future<void> _saveFitness() async {
      var prefs = await SharedPreferences.getInstance();
      prefs.setString('fitness_value', fitnessValue.toString());
    }
  }
}
